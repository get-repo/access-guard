<?php

if (
    ($_SERVER['REQUEST_TIME'] ?? false)
    && 'cli' !== php_sapi_name()
    && $param = getenv('ACCESS_GUARD_QUERY_PARAM')
) {
    $key = sprintf('_access_guard_query_param_%s', $param);
    session_start();
    if (array_key_exists($param, $_GET)) {
        $_SESSION[$key] = 1;
    } elseif (!isset($_SESSION[$key])) {
        http_response_code(429);
        exit;
    }
}
unset($key, $param);
