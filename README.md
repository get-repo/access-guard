<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/54916036/access-guard-logo.png" height=100 />
</p>

<h1 align=center>Access Query Checker</h1>

A very lightweight PHP project designed to effortlessly restrict website access based on a simple GET parameter in the URL.<br/>

## Table of Contents

1. [How It Works](#how-it-works)
1. [Use Case](#use-case)
1. [Installation](#installation)
1. [Symfony Integration](#symfony-integration)

<br/><br/>

## How It Works
Imagine you have a website where you can identify who the users are and who's accessing it.<br/>
You can prevent both random curious users and automated bots from accessing the website :<br/>

This library restricts website access just by appending a designated GET parameter to your website's URL.<br/>
Once the parameter is set, the website's session remembers it, ensuring that only individuals with knowledge of this parameter can access the site.
If the parameter is not set, the website's returns the HTTP code 429 (indicates the client has surpassed its rate limit... which is fake).

<br/>

## Use Case
Prevent random access to your website, robots and unwanted curious users.
And your website logs are smaller and cleaner.

<br/>

## Installation
 - This is installable via [Composer](https://getcomposer.org/):
   ```bash
   composer config repositories.get-repo/access-guard git https://gitlab.com/get-repo/access-guard.git
   composer require get-repo/access-guard
   ```
 - Define the name of the access parameter through an environment variable called `ACCESS_GUARD_QUERY_PARAM`.<br/>
    Example for apache server *(where **example** is the GET parameter name)* :
    ```xml
    <VirtualHost>
        ...
        SetEnv ACCESS_GUARD_QUERY_PARAM example
    </VirtualHost>
    ```

<br/>

## Symfony Integration
 - Define a parameter that is referencing the `ACCESS_GUARD_QUERY_PARAM` environment variable
    ```yaml
    parameters:
        getrepo_accessguard.param: '%env(ACCESS_GUARD_QUERY_PARAM)%'
    ```
 - Use this parameter to make sure the security component understands this strategy when calling the logout route :
    ```yaml
    security:
        firewalls:
            main:
                logout:
                    target: '/?%getrepo_accessguard.param%'
    ```
